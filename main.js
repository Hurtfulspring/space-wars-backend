require('dotenv').config();
const express = require('express');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', require('./routes/api'));

app.listen(process.env.PORT || 3000, () => {
  console.log('Listen to port ' + process.env.PORT || 3000);
});